<html class="no-js" <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>./style.css">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <title><?php bloginfo(); ?></title>
</head>

<section class="top container mt-5">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <img class="logo" src="<?php bloginfo('template_url'); ?>/inc/images/Mountain-Conqueror.png" />
      <div class="vertical-menu mt-5">
          <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <h1 class="header-text">»Life is a journey, not a destination.«<br> <span class="author-name">Ralph Waldo Emerson<span>  </h1>
