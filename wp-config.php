<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'inpsyde' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ucZ[S]N4Vv80F2Y_3>Cumm=O|[*.|+!xZ5fJsV],#tE@@J$i6BxF_k?}yQ#[o3~ ' );
define( 'SECURE_AUTH_KEY',  'SnRQ#B 3CLWjB&BWmn1?B._`j3VsAWY`ebN$^IfUrGP-uvM$.T,!3U]CbIjZ?|rT' );
define( 'LOGGED_IN_KEY',    '2wu]UfPR^K)2)7_6hIgW@2k[yrpCdM3Vxu+]3,XC^1}f4guaZEbt|[Le_BPrY7g3' );
define( 'NONCE_KEY',        '`oFmt~->M<%H l7O3MACS-s4_jZu}?!|&hqju^- %>,&$tpL)u_duFb60L%mi-[E' );
define( 'AUTH_SALT',        'd3 C4_u~4V7{{#~p>EdJMqQz nBw;-kX+5&x+#tr8Df_7f/&Bjc[D$QhnaRsRXed' );
define( 'SECURE_AUTH_SALT', 'K0(9FUvDBcONAB7D$^WL.C O+<xY6i8X{v*:uMGL2jiEIzC7hQM1xeVts9>Z_4LN' );
define( 'LOGGED_IN_SALT',   '>VOK]*5xBjPQZ%oJvqN^D4okE^}Tb%lxW>Brc,S.5m?0|ew?}L?`.&Z3G{[M4+@5' );
define( 'NONCE_SALT',       'e}0kGuO8V%l:0~RMENI_JC}!zK(milV$Dtl}`Y+iR5p?#L}yG)PK {47DygKLkB ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
